/* src/main.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <search.hpp>
#include <search_generator.hpp>
#include <string>
#include <thread>
#include <vector>
#include <wx/listbox.h>
#include <wx/spinctrl.h>
#include <wx/wx.h>

//
// Generate list
//
static constexpr uint64_t SEED[2] = {0xa8b2f1fc252f3e2e, 0xfbb9b0e565b1979}; /// Random seed.
/**
 * @brief Generate random list.
 * @param length Length of the strings.
 * @return Generated random list.
 */
inline ::std::vector<::std::string> generate_values(size_t length = 4) {
	::std::vector<::std::string> result = ::search::generate_string_vector(length);
	::search::shuffle_string_vector(result, SEED);
	return result;
}
static ::std::vector<::std::string> values = generate_values(); /// Values to process.

//
// Gui
//
/**
 * @brief Main window.
 */
class MainFrame : public wxFrame {
	private:
		wxTextCtrl* _searchBar;    /// Search bar entry.
		wxStaticText* _totalTime;  /// Total time to print.
		wxStaticText* _searchTime; /// Search time to print.
		wxSpinCtrl* _threads;      /// Threads to use.
		wxListBox* _list;          /// List to insert results.

	public:
		/**
		 * @brief Construct a new main windows.
		 */
		MainFrame() : wxFrame(nullptr, wxID_ANY, "Prefix Search") {
			// Main container
			wxBoxSizer* main_box = new wxBoxSizer(wxVERTICAL);
			this->SetSizer(main_box);

			// Search
			{
				// Layout
				wxBoxSizer* box = new wxBoxSizer(wxHORIZONTAL);
				main_box->Add(box, 0, wxEXPAND);

				// Search string
				this->_searchBar = new wxTextCtrl(this, wxID_ANY, wxT("Search..."));
				box->Add(this->_searchBar, 1, wxEXPAND);

				// Search button
				wxButton* button = new wxButton(this, wxID_ANY, wxT("Search"));
				box->Add(button, 0, wxEXPAND);
				Connect(wxID_ANY, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::OnSearch));
			}

			// Status and threads
			{
				// Layout
				wxBoxSizer* box = new wxBoxSizer(wxHORIZONTAL);
				main_box->Add(box, 0, wxEXPAND);

				// Text output
				{
					box->Add(new wxStaticText(this, wxID_ANY, wxT("Total time: ")));
					this->_totalTime = new wxStaticText(this, wxID_ANY, wxT("…"));
					box->Add(this->_totalTime);
					box->Add(new wxStaticText(this, wxID_ANY, wxT("  |  Search time: ")));
					this->_searchTime = new wxStaticText(this, wxID_ANY, wxT("…"));
					box->Add(this->_searchTime);
				}

				// Space
				{
					wxStaticText* spacer = new wxStaticText(this, wxID_ANY, wxT(""));
					box->Add(spacer, 1, wxEXPAND);
				}

				// Threads choose
				{
					wxStaticText* threads_text = new wxStaticText(this, wxID_ANY, wxT("Threads: "));
					box->Add(threads_text, 0, wxEXPAND);
					this->_threads = new wxSpinCtrl(this, wxID_ANY, wxT("1"));
					this->_threads->SetRange(1, ::std::thread::hardware_concurrency());
					box->Add(this->_threads, 0, wxEXPAND);
				}
			}

			// Insert word list
			{
				this->_list = new wxListBox(this, wxID_ANY);
				main_box->Add(this->_list, 1, wxEXPAND);
			}
		}

	private:
		/**
		 * @brief Processes searching.
		 * @param event Event that happend. Not used.
		 */
		void OnSearch([[maybe_unused]] wxCommandEvent& event) {
			double time_res;

			// Start messure time
			::search::internal::prevent_reordering();
			auto time1 = ::std::chrono::high_resolution_clock::now();
			::search::internal::prevent_reordering();

			// Operate
			auto result = ::search::prefix_search_timed(values,
			                                            ::std::string_view(this->_searchBar->GetValue().c_str()),
			                                            time_res,
			                                            this->_threads->GetValue());

			// Stop messure time
			::search::internal::prevent_reordering();
			auto time2 = ::std::chrono::high_resolution_clock::now();
			::search::internal::prevent_reordering();

			// Output times
			this->_searchTime->SetLabel(wxString(::std::to_string(time_res).c_str()));
			this->_totalTime->SetLabel(
				wxString(::std::to_string(::std::chrono::duration<double>(time2 - time1).count()).c_str()));

			// Output list
			this->_list->Clear();
			for(auto& i: result) {
				this->_list->Append(wxString(i.c_str()));
			}

			// Update layout
			this->Layout();
		}
};

/**
 * @brief Main wx app.
 */
class MainApp : public wxApp {
	public:
		/**
		 * @brief Start up wx app.
		 */
		bool OnInit() override {
			MainFrame* frame = new MainFrame();
			frame->Show(true);
			return true;
		}
};

// Define main
wxIMPLEMENT_APP(MainApp);
