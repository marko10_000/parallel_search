/* include/search.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once
#include <array>
#include <atomic>
#include <chrono>
#include <concepts>
#include <condition_variable>
#include <functional>
#include <iterator>
#include <memory>
#include <mutex>
#include <ranges>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

/**
 * @brief Parallel string prefix search
 */
namespace search {
	//
	// String prefix test
	//
	/**
	 * @brief Concept to describe a string like type.
	 * @details Has to be an input range over chars that can be const, volatile and/or reference.
	 * @tparam TYPE Type to check.
	 */
	template<class TYPE>
	concept string_type = ::std::ranges::input_range<TYPE> && ::std::same_as < ::std::remove_cvref_t<::std::iter_value_t<TYPE>>,
	char > ;

	/**
	 * @brief Checks if a string has a given prefix.
	 * @tparam STRING Type of the string input ranger to check.
	 * @tparam PREFIX Type of the prefix input ranger to check.
	 * @param string String to check.
	 * @param prefix Prefix to use to check.
	 * @return True when string has the given prefix else false.
	 */
	template<::search::string_type STRING, ::search::string_type PREFIX>
	inline bool prefix_test(STRING&& string, PREFIX&& prefix) {
		// Find end and poisition
		auto s_end = ::std::ranges::end(string);
		auto p_end = ::std::ranges::end(prefix);
		auto s_pos = ::std::ranges::begin(string);
		auto p_pos = ::std::ranges::begin(prefix);

		// Compare entries
		while(!(s_pos == s_end) && !(p_pos == p_end)) {
			if(*s_pos != *p_pos) {
				return false;
			}
			s_pos++;
			p_pos++;
		}
		return p_pos == p_end;
	}

	//
	// Word list container
	//
	/**
	 * @brief Internal stuff of the module.
	 */
	namespace internal {
		/**
		 * @brief Support type to call private functions.
		 */
		struct CallHelper {
			public:
				/**
				 * @brief Call `_next_iteration` on type.
				 * @tparam TYPE Type to that should be called.
				 * @tparam ARGS Arguments to be parsed to the arguments.
				 * @param type Object to be called on.
				 * @param args Arguments to forward.
				 */
				template<class TYPE, class... ARGS>
				static void next_iteration(TYPE&& type, ARGS&&... args) {
					type._next_iteration(::std::forward<ARGS>(args)...);
				}
				/**
				 * @brief Call `_run_thread_job_prefix_search` on type.
				 * @tparam TYPE Type to that should be called.
				 * @tparam ARGS Arguments to be parsed to the arguments.
				 * @param type Object to be called on.
				 * @param args Arguments to forward.
				 */
				template<class TYPE, class... ARGS>
				static void run_thread_job_prefix_search(TYPE&& type, ARGS&&... args) {
					type._run_thread_job_prefix_search(::std::forward<ARGS>(args)...);
				}
		};

		/**
		 * @brief Prevent compiler reordering.
		 */
		inline void prevent_reordering() {
			asm volatile("" ::: "memory");
		}
	} // namespace internal

	/**
	 * @brief Concept to descrip a container strings with random access.
	 * @tparam TYPE Type to check.
	 */
	template<class TYPE>
	concept string_container_type = ::std::ranges::random_access_range<TYPE> && ::search::string_type<::std::iter_value_t<TYPE>>;

	/**
	 * @brief Get the type of string that is containt in a container.
	 * @tparam TYPE Container type to analyse.
	 */
	template<::search::string_container_type TYPE>
	using container_type = ::std::iter_value_t<TYPE>;

	/**
	 * @brief A block of in a word list.
	 * @tparam CONTENT Type of string reference that should be stored.
	 * @tparam BLOCK_SIZE Size of the block. Default 32.
	 */
	template<::search::string_type CONTENT, size_t BLOCK_SIZE = 32>
	struct WordList_Block final {
		public:
			::std::array<CONTENT*, BLOCK_SIZE> content;    /// Content that is referenced.
			::std::array<size_t, BLOCK_SIZE> indexes;      /// Indexes of the referenced content.
			size_t size                         = 0;       /// Length of the content
			size_t iteration                    = 0;       /// Iteration of the word list for synchronisation
			::std::atomic<WordList_Block*> next = nullptr; /// Next block
	};

	/**
	 * @brief The word list storing result of multi threaded search. Linked list implementation.
	 * @tparam CONTENT Type of string that is referenced.
	 * @tparam BLOCK_SIZE Block size of the list. Default 32.
	 * @tparam ALLOCATOR Allocator used to generate WordList_block-s. Default std allocator.
	 */
	template<::search::string_type CONTENT,
	         size_t BLOCK_SIZE = 32,
	         class ALLOCATOR   = ::std::allocator<::search::WordList_Block<CONTENT, BLOCK_SIZE>>>
	class WordList final {
		private:
			using _WLB = ::search::WordList_Block<CONTENT, BLOCK_SIZE>; /// Type definition of WordList_Block

			::std::vector<_WLB> _list_begin;      /// List begin of each thread's list.
			::std::vector<_WLB*> _list_end;       /// List of pointer to the end of each thread's list.
			ALLOCATOR _allocator;                 /// Allocator used.
			::std::atomic<size_t> _iteration = 1; /// Current iteration used to synchronice iterator and worker threads.
			size_t _size                     = 0; /// Size of the list synchronized for iteration (could be overestimated).
			::std::atomic<size_t> _size_next = 0; /// Size of the list stored for the next synchronisation.

			// Allocator
			/**
			 * @brief Helper function to allocate a new block.
			 * @param iteration Current iteration id.
			 * @return Next block to use.
			 */
			_WLB* _allocator_alloc(size_t iteration) {
				_WLB* result      = this->_allocator.allocate(1);
				result->iteration = iteration;
				result->next      = nullptr;
				result->size      = 0;
				return result;
			}
			/**
			 * @brief Helper function to destroy a block with the allocator.
			 * @param content Block that should be destroyed.
			 */
			void _allocator_destroy(_WLB* content) {
				::std::destroy_at(content);
				this->_allocator.deallocate(content, 1);
			}

			/**
			 * @brief Runs a thread job of processing prefixes and insert matching elements to the word lists. Will be called by
			 * WordProcessor.
			 * @warning Requires that an atomic thread fence was applied for synchronisation of container content, prefix and word
			 * list constructor.
			 * @warning Requires that an atomic thread fence was applied for synchronisation of the word list should differend
			 * thread use the same thread_id.
			 * @tparam CONTAINER Type of the container that should be processed.
			 * @tparam PREFIX Type of the prefix to compare to.
			 * @param container Container of the strings to process.
			 * @param container_start Start position in the container to start (inclusive).
			 * @param container_end End position in the container to stop (exklusive).
			 * @param prefix Prefix to test for.
			 * @param thread_id Id of the thread that is currently running.
			 */
			template<::search::string_container_type CONTAINER, ::search::string_type PREFIX>
			requires ::std::same_as<::std::remove_cvref_t<::search::container_type<CONTAINER>>, ::std::remove_cvref_t<CONTENT>>
			void _run_thread_job_prefix_search(CONTAINER&& container,
			                                   size_t container_start,
			                                   size_t container_end,
			                                   PREFIX&& prefix,
			                                   size_t thread_id) {
				// Get data
				size_t iteration       = this->_iteration.load();
				size_t int_pos         = container_start;
				auto c_pos             = ::std::ranges::begin(container) + container_start;
				auto c_end             = ::std::ranges::begin(container) + container_end;
				_WLB* insert_container = this->_list_end.at(thread_id);
				size_t added           = 0;

				// Process content
				while(!(c_pos == c_end)) {
					if(::search::prefix_test(*c_pos, prefix)) {
						// Check if insert container is usable
						if(insert_container->iteration != iteration) {
							// Old iteration, requires new block with atomic fencing
							_WLB* next = this->_allocator_alloc(iteration);
							::std::atomic_thread_fence(::std::memory_order_release);
							insert_container->next.store(next);
							insert_container = next;
						}
						if(insert_container->size >= BLOCK_SIZE) {
							// Block full, generat new (doesn't indanger iterator in other thread)
							_WLB* next = this->_allocator_alloc(iteration);
							insert_container->next.store(next, ::std::memory_order_relaxed);
							insert_container = next;
						}

						// Insert content
						insert_container->content[insert_container->size] = &(*c_pos);
						insert_container->indexes[insert_container->size] = int_pos;
						insert_container->size++;
						added++;
					}
					c_pos++;
					int_pos++;
				}

				// Finish task
				this->_list_end[thread_id] = insert_container;
				this->_size_next.fetch_add(added);
			}

			/**
			 * @brief Moves the word list to the next iteration. No thread should be working in that time on it.
			 */
			void _next_iteration() {
				this->_iteration.fetch_add(1);
				this->_size = this->_size_next.load();
			}

		public:
			/**
			 * @brief Iterator of the thread content.
			 */
			struct iterator final {
				private:
					static constexpr size_t _no_thread
						= ~static_cast<size_t>(0); /// Default value when not thread could be found.

					::std::vector<_WLB*> _blocks           = {}; /// Referenced blocks
					::std::vector<size_t> _blocks_position = {}; /// Index inside of the block.
					size_t _current_thread                 = 0;  /// Current thread id of the value.
					size_t _iteration                      = 0;  /// Iteration for synchronized.

					/**
					 * @brief Get thread id of the current value.
					 * @tparam SAME_VALUE When true should only search the current value (start value), else search next value.
					 * @param blocks Thread blocks to search in. Will be updated to the current value.
					 * @param block_position Position inside of the thread blocks. Will be updated to the curent value.
					 * @param current_index Current source list index.
					 * @param iteration Iteration id of safe usage.
					 * @return Thread id inside of the given block.
					 */
					template<bool SAME_VALUE>
					static size_t _get_thread_id(::std::vector<_WLB*>& blocks,
					                             ::std::vector<size_t>& block_position,
					                             size_t current_index,
					                             size_t iteration) {
						// Process input
						size_t result     = _no_thread;
						size_t next_index = ~static_cast<size_t>(0);
						for(size_t tid = 0; tid < blocks.size(); tid++) {
							if(blocks[tid] != nullptr) {
								// Process content
								_WLB* wlb      = blocks[tid];
								size_t wlb_pos = block_position[tid];
								while(wlb != nullptr) {
									// Block is in the future iteration?
									// Have to be checked first to never access any other value is synchronized.
									if(wlb->iteration > iteration) {
										wlb = nullptr;
										break;
									}

									if(wlb_pos < wlb->size) {
										if constexpr(SAME_VALUE) {
											// Check current position only
											if(wlb->indexes[wlb_pos] <= next_index) {
												next_index = wlb->indexes[wlb_pos];
												result     = tid;
											}
											break;
										}
										else {
											// Check if current value is valid
											if(wlb->indexes[wlb_pos] > current_index) {
												if(wlb->indexes[wlb_pos] < next_index) {
													// Found better index, use it
													next_index = wlb->indexes[wlb_pos];
													result     = tid;
												}
												break;
											}
											else {
												// Try to use next value
												wlb_pos++;
												if(wlb_pos < wlb->size) {
													if(wlb->indexes[wlb_pos] <= current_index) {
														throw ::std::logic_error(
															"Implementation error."); // Should never happen. Indexes out of
														                              // order.
													}
													if(wlb->indexes[wlb_pos] < next_index) {
														// Found next best entry
														next_index = wlb->indexes[wlb_pos];
														result     = tid;
													}
													break;
												}
											}
										}
									}

									// Next block
									wlb     = wlb->next;
									wlb_pos = 0;
								}

								// Update value
								blocks[tid]         = wlb;
								block_position[tid] = (wlb == nullptr) ? 0 : wlb_pos;
							}
						}

						// Check result if it's end
						if(result == _no_thread) {
							blocks.clear();
							block_position.clear();
						}
						return result;
					}

				public:
					using difference_type   = ssize_t;                 /// Differenz type.
					using iterator_category = std::input_iterator_tag; /// Is only input iterator.
					using pointer           = CONTENT*;                /// Pointer type of iteration value
					using reference         = CONTENT&;                /// Reference type of iteration value
					using value_type        = CONTENT;                 /// Value type of iteration value

					/**
					 * @brief Default constructor support.
					 */
					iterator() {}
					~iterator() noexcept {}

					/**
					 * @brief Compares two iterators position.
					 * @param iter Iterator to compare to.
					 * @return true when it's the same position else false.
					 * @{
					 */
					bool operator==(const iterator& iter) const {
						return (this->_blocks == iter._blocks) && (this->_blocks_position == this->_blocks_position);
					}
					bool operator!=(const iterator& iter) const {
						return !((*this) == iter);
					}
					/// @}

					/**
					 * @brief Value access.
					 * @throws ::std::out_of_range When iterator is at the end of iteration.
					 * @return Reference to the value.
					 */
					CONTENT& operator*() const {
						return *const_cast<CONTENT*>(
							this->_blocks[this->_current_thread]->content[this->_blocks_position[this->_current_thread]]);
					}

					/**
					 * @brief Iterate to the next value.
					 * @return The current iterator as a reference.
					 * @throws ::std::out_of_range Iterate over the end.
					 */
					iterator& operator++() {
						// Check if end
						if(this->_blocks.size() == 0) {
							throw ::std::out_of_range("When iterator is at the end of iteration.");
						}

						// Got to the next value
						this->_current_thread = iterator::_get_thread_id<false>(
							this->_blocks,
							this->_blocks_position,
							this->_blocks[this->_current_thread]->indexes[this->_blocks_position[this->_current_thread]],
							this->_iteration);

						// Done, return self
						return *this;
					}
					/**
					 * @brief Iterate to the next value.
					 * @param value Values to step. Have to be >= 0.
					 * @return The current iterator as a reference.
					 * @throws ::std::out_of_range Iterate
					 * over the end.
					 */
					iterator& operator++(int value) {
						// Check value
						if(value < 0) {
							throw ::std::out_of_range("Negative iterations aren't possible.");
						}

						// Do operations
						for(int i = 0; i <= value; i++) {
							this->operator++();
						}

						// Done, return self
						return *this;
					}

					// Checks and friends
					friend WordList;
			};

			/**
			 * @brief Default constructor support of a word list.
			 */
			WordList() {}
			/**
			 * @brief Implementation of the move constructor.
			 * @param wl Value that should be moved.
			 */
			WordList(WordList&& wl) :
				_list_begin(::std::move(wl._list_begin)),
				_list_end(::std::move(wl._list_end)),
				_allocator(::std::move(wl._allocator)),
				_iteration(wl._iteration.load()),
				_size(wl._size),
				_size_next(wl._size_next.load()) {}
			/**
			 * @brief Construct a new word list object depends on thread amount.
			 * @param threads Amount of threads used.
			 * @param allocator Allocator used.
			 */
			WordList(size_t threads, ALLOCATOR allocator = {}) :
				_list_begin(::std::vector<_WLB>(threads)), _list_end(threads, nullptr), _allocator(::std::move(allocator)) {
				for(size_t index = 0; index < threads; index++) {
					this->_list_end[index] = &(this->_list_begin[index]);
				}
			}
			/**
			 * @brief Destroy a word list object.
			 * @warning Requires synchronisation of worker threads.
			 */
			~WordList() {
				for(auto& head: this->_list_begin) {
					_WLB* current = head.next;
					while(current != nullptr) {
						_WLB* next = current->next.load(::std::memory_order_relaxed);
						this->_allocator_destroy(current);
						current = next;
					}
				}
			}

			// Remove copy constructor & setting
			WordList(const WordList&)            = delete;
			WordList& operator=(const WordList&) = delete;

			/**
			 * @brief Iterator access of the start of the list.
			 * @return Start position of iterator.
			 */
			iterator begin() {
				iterator result;
				result._blocks = ::std::move(::std::vector<_WLB*>(this->_list_begin.size(), nullptr));
				for(size_t tid = 0; tid < this->_list_begin.size(); tid++) {
					result._blocks[tid] = &(this->_list_begin[tid]);
				}
				result._blocks_position = ::std::move(::std::vector<size_t>(this->_list_begin.size(), 0));
				result._iteration       = ::std::move(this->_iteration.load() - 1);
				result._current_thread
					= iterator::template _get_thread_id<true>(result._blocks, result._blocks_position, 0, result._iteration);
				return result;
			}
			/**
			 * @brief Iterator access of the end of the list.
			 * @return End position of iterator.
			 */
			iterator end() {
				return {};
			}

			/**
			 * @brief Gain access to the current stored amount of entires.
			 * @details Result is equal or bigger than the amount of elements when the iterator outputs as long as it's in the
			 * same iteration.
			 * @warning Can be overestimated when input get sill processed.
			 * @return Curent size.
			 */
			size_t size() const {
				return this->_size;
			}
			/**
			 * @brief Returns the amount of supported threads.
			 * @return Supported threads.
			 */
			size_t threads_supported() const {
				return this->_list_begin.size();
			}

			// Friends
			friend ::search::internal::CallHelper;
	};

	/**
	 * @brief Word list multi threader worker. Manages the thread load on word lists.
	 * @tparam WORD_LIST Type of wordlist to work on.
	 * @tparam SRC_CONTAINER Type of the source container to accept.
	 * @tparam PREFIX Type of prefix to use as input.
	 */
	template<class WORD_LIST, ::search::string_container_type SRC_CONTAINER, ::search::string_type PREFIX>
	class WordListWorker;
	template<::search::string_type CONTENT,
	         size_t BLOCK_SIZE,
	         ::search::string_container_type SRC_CONTAINER,
	         ::search::string_type PREFIX>
	class WordListWorker<::search::WordList<CONTENT, BLOCK_SIZE>, SRC_CONTAINER, PREFIX> final {
		private:
			using _WL = ::search::WordList<CONTENT, BLOCK_SIZE>; /// Type defintion of the word list.

			// Threads organisation
			::std::atomic<size_t> _thread_count = 0;  /// Amounts of active threads.
			::std::atomic_flag _thread_stop     = {}; /// Flag that the threads should stop waiting for work.

			// Source and target container management
			::std::atomic<SRC_CONTAINER*> _src_container = nullptr; /// Container that contains the src list to process.
			::std::atomic<size_t> _src_container_pos     = 0; /// Position inside of the source container for the working threads.
			::std::atomic<size_t> _src_container_steps   = 0; /// Steps size per position to make.
			::std::atomic<PREFIX*> _src_prefix           = nullptr; /// Prefix that should be processed.
			::std::atomic<_WL*> _target                  = nullptr; /// Target world list that should be updated.

			// Work organisation
			::std::atomic<size_t> _working             = 0;  /// Amount of working threads.
			::std::mutex _work_signal_mutex            = {}; /// Singal mutex to manage stage changes.
			::std::condition_variable_any _work_signal = {}; /// Signal to notify that new work was added.
			::std::atomic_flag _work_avaliable         = {}; /// Flag to identify that new work is avaliable.

			// Work done
			::std::atomic_flag _work_done                   = {}; /// Flag to check if work it self is done.
			::std::condition_variable_any _work_done_signal = {}; /// Signal to await for till all work is done.

			/**
			 * @brief Stops and awaits till all workers stoped working.
			 */
			void _stop_workers() {
				{
					::std::lock_guard g(this->_work_signal_mutex);
					this->_work_avaliable.clear();
				}
				while(this->_working.load() != 0) {} // Wait for thread termination
			}

		public:
			/**
			 * @brief Default constructor of a word list worker.
			 */
			WordListWorker() {
				this->_thread_stop.clear();
				this->_work_avaliable.clear();
			}
			/**
			 * @brief Terminate the threads and destroy the word list worker.
			 */
			~WordListWorker() {
				this->exit();
			}

			// Disallow copy and moving
			WordListWorker(const WordListWorker&)            = delete;
			WordListWorker(WordListWorker&&)                 = delete;
			WordListWorker& operator=(const WordListWorker&) = delete;
			WordListWorker& operator=(WordListWorker&&)      = delete;

			/**
			 * @brief Applies a prefix job to the workers.
			 * @param target Target wordlist to write to.
			 * @param src Source container to read from.
			 * @param prefix Prefix to search from.
			 * @param stepSize The size of the steps per thread.
			 */
			void apply_prefix_job(_WL& target, SRC_CONTAINER& src, PREFIX& prefix, size_t stepSize = 100) {
				// Stop workers from last job
				this->_stop_workers();

				// Add new job
				::std::atomic_thread_fence(::std::memory_order_release);
				this->_src_container.store(&src);
				this->_src_container_pos.store(0);
				this->_src_container_steps.store(stepSize);
				this->_src_prefix.store(&prefix);
				this->_target.store(&target);

				// Start threads
				{
					::std::lock_guard g(this->_work_signal_mutex);
					this->_work_done.clear();
					this->_work_avaliable.test_and_set();
					this->_work_signal.notify_all();
				}
			};

			/**
			 * @brief Awaits for that the job will be done.
			 */
			void await_workers_done() {
				// Await job is done
				::std::lock_guard g(this->_work_signal_mutex);
				while(!this->_work_done.test()) {
					this->_work_done_signal.wait(this->_work_signal_mutex);
				}

				// Synchronice memory
				::std::atomic_thread_fence(::std::memory_order_acquire);
				::search::internal::CallHelper::next_iteration(*(this->_target.load()));
			}

			/**
			 * @brief Stop working pool.
			 */
			void exit() {
				// Stop workers
				this->_thread_stop.test_and_set();
				this->_stop_workers();

				// Kill sleeping workers
				{
					::std::lock_guard g(this->_work_signal_mutex);
					this->_work_signal.notify_all();
				}
				while(this->_thread_count.load() != 0) {}
			}

			/**
			 * @brief Thread joins the worker threads.
			 */
			void thread_join_pool() {
				// Join pool
				size_t t_id = this->_thread_count.fetch_add(1);

				// Work processing
				while(true) {
					// Get work or exit main loop
					{
						::std::lock_guard g(this->_work_signal_mutex);
						bool is_work_avaliable = false;
						while(!is_work_avaliable) {
							// Check if threads should stop working/exit pool.
							if(this->_thread_stop.test()) {
								goto EXIT_OUTER_LOOP;
							}

							// Check if work is avaliable
							if(this->_work_avaliable.test()) {
								// Add yourself to working
								this->_working.fetch_add(1);
								is_work_avaliable = true;
								::std::atomic_thread_fence(::std::memory_order_acquire);
							}
							else {
								// Await for signal
								this->_work_signal.wait(this->_work_signal_mutex);
							}
						}
					}

					// Found work
					try {
						SRC_CONTAINER* src = this->_src_container.load();
						PREFIX* prefix     = this->_src_prefix.load();
						_WL* target        = this->_target.load();
						if(target->threads_supported() > t_id) {
							// Allowed to work, because thread id is supported
							size_t workload = this->_src_container_steps.load();
							while(this->_work_avaliable.test()) {
								size_t pos = this->_src_container_pos.fetch_add(workload);
								if(pos < src->size()) {
									::search::internal::CallHelper::run_thread_job_prefix_search(
										*target,
										*src,
										pos,
										((pos + workload < src->size()) ? pos + workload : src->size()),
										*prefix,
										t_id);
								}
								else {
									::std::lock_guard g(this->_work_signal_mutex);
									this->_work_avaliable.clear();
								}
							}

							// Make result avaliable and notify awaiter
							{
								::std::lock_guard g(this->_work_signal_mutex);
								::std::atomic_thread_fence(::std::memory_order_release);
								if(this->_working.fetch_sub(1) == 1) {
									this->_work_done.test_and_set();
									this->_work_done_signal.notify_all();
								}
							}
						}
					}
					catch(...) { // Fix working and thread counter.
						this->_working.fetch_sub(1);
						this->_thread_count.fetch_sub(1);
						throw;
					}
				}
EXIT_OUTER_LOOP:;

				// Fix thread count
				this->_thread_count.fetch_sub(1);
			}
	};

	/**
	 * @brief Makes a prefix search on an list of string like objects.
	 * @tparam BLOCK_SIZE Size of a world list block.
	 * @tparam CONTAINER Type of the container that contains the strings.
	 * @tparam PREFIX Type of the prefix to process.
	 * @param container Container to process.
	 * @param prefix Prefix to search for.
	 * @param time Output reference for the required time (without tearup and -down).
	 * @param threads Amount of threads to utilize. When 0-1 then it will be executed sequential. Default value run a much threads
	 * as the hardware allow.
	 * @param stepSize Size of the steps per thread. Only importent for multi threaded context.
	 * @return List of words that match.
	 */
	template<size_t BLOCK_SIZE = 32, ::search::string_container_type CONTAINER, ::search::string_type PREFIX>
	::search::WordList<::search::container_type<CONTAINER>, BLOCK_SIZE> prefix_search_timed(
		CONTAINER& container,
		PREFIX&& prefix,
		double& time,
		size_t threads  = ~static_cast<size_t>(0),
		size_t stepSize = 100) {
		// Defintion
		using ResultType = ::search::WordList<::search::container_type<CONTAINER>, BLOCK_SIZE>;
		using WLW = ::search::WordListWorker<ResultType, ::std::remove_reference_t<CONTAINER>, ::std::remove_reference_t<PREFIX>>;

		// Check thread amount
		if(threads == ~static_cast<size_t>(0)) {
			threads = ::std::thread::hardware_concurrency();
		}

		if(threads < 2) {
			// Sequential execution
			ResultType result(1);

			// Start messure time
			::search::internal::prevent_reordering();
			auto time1 = ::std::chrono::high_resolution_clock::now();
			::search::internal::prevent_reordering();

			// Process content
			::search::internal::CallHelper::run_thread_job_prefix_search(result, container, 0, container.size(), prefix, 0);
			::search::internal::CallHelper::next_iteration(result);

			// Stop messure time
			::search::internal::prevent_reordering();
			auto time2 = ::std::chrono::high_resolution_clock::now();
			::search::internal::prevent_reordering();

			// Return result
			time = ::std::chrono::duration<double>(time2 - time1).count();
			return result;
		}
		else {
			// Parallel execution
			// Setup thread pool
			::std::vector<::std::thread> ths(threads);
			WLW wlw;
			ResultType result(threads);
			for(size_t t = 0; t < threads; t++) {
				ths[t] = ::std::thread([](WLW* wlw) -> void { wlw->thread_join_pool(); }, &wlw);
			}

			// Start messure time
			::search::internal::prevent_reordering();
			auto time1 = ::std::chrono::high_resolution_clock::now();
			::search::internal::prevent_reordering();

			// Process content
			wlw.apply_prefix_job(result, container, prefix, stepSize);
			wlw.await_workers_done();

			// Stop messure time
			::search::internal::prevent_reordering();
			auto time2 = ::std::chrono::high_resolution_clock::now();
			::search::internal::prevent_reordering();

			// Teardown
			wlw.exit();
			for(auto& t: ths) {
				t.join();
			}

			// Return value
			time = ::std::chrono::duration<double>(time2 - time1).count();
			return result;
		}
	}
	/**
	 * @brief Makes a prefix search on an list of string like objects.
	 * @tparam BLOCK_SIZE Size of a world list block.
	 * @tparam CONTAINER Type of the container that contains the strings.
	 * @tparam PREFIX Type of the prefix to process.
	 * @param container Container to process.
	 * @param prefix Prefix to search for.
	 * @param threads Amount of threads to utilize. When 0-1 then it will be executed sequential. Default value run a much threads
	 * as the hardware allow.
	 * @param stepSize Size of the steps per thread. Only importent for multi threaded context.
	 * @return List of words that match.
	 */
	template<size_t BLOCK_SIZE = 32, ::search::string_container_type CONTAINER, ::search::string_type PREFIX>
	::search::WordList<::search::container_type<CONTAINER>, BLOCK_SIZE> prefix_search(CONTAINER& container,
	                                                                                  PREFIX&& prefix,
	                                                                                  size_t threads  = ~static_cast<size_t>(0),
	                                                                                  size_t stepSize = 100) {
		double _;
		return ::search::prefix_search_timed<BLOCK_SIZE>(container, prefix, _, threads, stepSize);
	}
} // namespace search
