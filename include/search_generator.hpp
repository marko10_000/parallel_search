/* include/search_generator.hpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once
#include <array>
#include <cstdint>
#include <string>
#include <vector>

namespace search {
	/**
	 * @brief XorshiftPlus 128 bit pseudorandom number generator.
	 */
	struct XorshiftPlus128 {
		public:
			uint64_t state[2]; /// State of the generator

			/**
			 * @brief Access the current value of the generator.
			 * @return Value of the generator.
			 */
			uint64_t current() const {
				return this->state[0] + this->state[1];
			}

			/**
			 * @brief Step one step to the next value.
			 */
			void next() {
				uint64_t mod   = this->state[0];
				this->state[0] = this->state[1];
				mod ^= mod << 23;
				mod ^= mod >> 18;
				mod ^= this->state[0] ^ (this->state[0] >> 5);
				this->state[1] = mod;
			}
	};

	/**
	 * @brief Generate a vector of strings with all of combinations the length from A-Z.
	 * @param length Length of every string.
	 * @return Generated string list.
	 */
	inline ::std::vector<::std::string> generate_string_vector(uint64_t length) {
		// Calculate size and allocate result vector
		::std::vector<::std::string> result = {};
		size_t size                         = 0;
		{
			for(size_t iter = 0; iter < length; iter++) {
				if(iter == 0) {
					size = 'Z' - 'A' + 1;
				}
				else {
					size *= 'Z' - 'A' + 1;
				}
			}
			result.reserve(size);
		}

		// Generate output
		{
			::std::string current(length, 'A');
			for(size_t pos = 0; pos < size; pos++) {
				result.push_back(current);

				// Update current to next value
				for(size_t current_pos = 0; current_pos < length; current_pos++) {
					current[current_pos]++;
					if(current[current_pos] > 'Z') {
						current[current_pos] = 'A';
					}
					else {
						break;
					}
				}
			}
		}

		// Done
		return result;
	}

	/**
	 * @brief Shuffles a string vector.
	 * @param to_shuffle Vector that should be shuffled. Value of the vector will updated.
	 * @param seed Seed of the random generator.
	 * @warning Values of the seed aren't allowed to be both 0.
	 */
	inline void shuffle_string_vector(::std::vector<::std::string>& to_shuffle, const uint64_t seed[2]) {
		// Initialize generator
		::search::XorshiftPlus128 generator = {};
		generator.state[0]                  = seed[0];
		generator.state[1]                  = seed[1];

		// Shuffle
		if((to_shuffle.size() | 1) == 1) {
			for(size_t index = 0; index < to_shuffle.size(); index++) {
				generator.next();
				size_t new_pos = generator.current() % to_shuffle.size();
				::std::swap(to_shuffle[index], to_shuffle[new_pos]);
			}
		}
		else {
			// Special case in even size to improve entropy usage.
			size_t modulo = to_shuffle.size() | 1;
			for(size_t index = 0; index < to_shuffle.size(); index++) {
				generator.next();
				size_t new_pos = generator.current() % to_shuffle.size();
				if(new_pos == modulo) {
					new_pos = 0; // Overshoot, map to the beginning of the field.
				}
				::std::swap(to_shuffle[index], to_shuffle[new_pos]);
			}
		}
	}
} // namespace search
