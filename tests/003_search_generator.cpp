/* tests/003_search_generator.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <algorithm>
#include <search_generator.hpp>

#include <catch2/catch.hpp>
#include <cstdint>
#include <string>
#include <unordered_set>
#include <vector>

using ::std::string;
using ::std::vector;

//
// String list generation tests
//
TEST_CASE("Test string vector generator", "[generate_string_vector]") {
	for(size_t length = 1; length < 5; length++) {
		vector<string> list = ::search::generate_string_vector(length);
		size_t size         = 1;
		for(size_t i = 0; i < length; i++) {
			size *= 26;
		}
		REQUIRE(list.size() == size);
	}
}

TEST_CASE("Test string vector generator of size 1", "[generate_string_vector][size 1]") {
	vector<string> list = ::search::generate_string_vector(1);
	REQUIRE(list.size() == 26);
	size_t pos = 0;
	for(auto i: {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
	             "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}) {
		REQUIRE(list[pos] == i);
		pos++;
	}
}

TEST_CASE("Test string vector generator of size 2", "[generate_string_vector][size 2]") {
	vector<string> list = ::search::generate_string_vector(2);
	REQUIRE(list.size() == 26 * 26);
	size_t pos = 0;
	for(auto i:
	    {"AA", "BA", "CA", "DA", "EA", "FA", "GA", "HA", "IA", "JA", "KA", "LA", "MA", "NA", "OA", "PA", "QA", "RA", "SA", "TA",
	     "UA", "VA", "WA", "XA", "YA", "ZA", "AB", "BB", "CB", "DB", "EB", "FB", "GB", "HB", "IB", "JB", "KB", "LB", "MB", "NB",
	     "OB", "PB", "QB", "RB", "SB", "TB", "UB", "VB", "WB", "XB", "YB", "ZB", "AC", "BC", "CC", "DC", "EC", "FC", "GC", "HC",
	     "IC", "JC", "KC", "LC", "MC", "NC", "OC", "PC", "QC", "RC", "SC", "TC", "UC", "VC", "WC", "XC", "YC", "ZC", "AD", "BD",
	     "CD", "DD", "ED", "FD", "GD", "HD", "ID", "JD", "KD", "LD", "MD", "ND", "OD", "PD", "QD", "RD", "SD", "TD", "UD", "VD",
	     "WD", "XD", "YD", "ZD", "AE", "BE", "CE", "DE", "EE", "FE", "GE", "HE", "IE", "JE", "KE", "LE", "ME", "NE", "OE", "PE",
	     "QE", "RE", "SE", "TE", "UE", "VE", "WE", "XE", "YE", "ZE", "AF", "BF", "CF", "DF", "EF", "FF", "GF", "HF", "IF", "JF",
	     "KF", "LF", "MF", "NF", "OF", "PF", "QF", "RF", "SF", "TF", "UF", "VF", "WF", "XF", "YF", "ZF", "AG", "BG", "CG", "DG",
	     "EG", "FG", "GG", "HG", "IG", "JG", "KG", "LG", "MG", "NG", "OG", "PG", "QG", "RG", "SG", "TG", "UG", "VG", "WG", "XG",
	     "YG", "ZG", "AH", "BH", "CH", "DH", "EH", "FH", "GH", "HH", "IH", "JH", "KH", "LH", "MH", "NH", "OH", "PH", "QH", "RH",
	     "SH", "TH", "UH", "VH", "WH", "XH", "YH", "ZH", "AI", "BI", "CI", "DI", "EI", "FI", "GI", "HI", "II", "JI", "KI", "LI",
	     "MI", "NI", "OI", "PI", "QI", "RI", "SI", "TI", "UI", "VI", "WI", "XI", "YI", "ZI", "AJ", "BJ", "CJ", "DJ", "EJ", "FJ",
	     "GJ", "HJ", "IJ", "JJ", "KJ", "LJ", "MJ", "NJ", "OJ", "PJ", "QJ", "RJ", "SJ", "TJ", "UJ", "VJ", "WJ", "XJ", "YJ", "ZJ",
	     "AK", "BK", "CK", "DK", "EK", "FK", "GK", "HK", "IK", "JK", "KK", "LK", "MK", "NK", "OK", "PK", "QK", "RK", "SK", "TK",
	     "UK", "VK", "WK", "XK", "YK", "ZK", "AL", "BL", "CL", "DL", "EL", "FL", "GL", "HL", "IL", "JL", "KL", "LL", "ML", "NL",
	     "OL", "PL", "QL", "RL", "SL", "TL", "UL", "VL", "WL", "XL", "YL", "ZL", "AM", "BM", "CM", "DM", "EM", "FM", "GM", "HM",
	     "IM", "JM", "KM", "LM", "MM", "NM", "OM", "PM", "QM", "RM", "SM", "TM", "UM", "VM", "WM", "XM", "YM", "ZM", "AN", "BN",
	     "CN", "DN", "EN", "FN", "GN", "HN", "IN", "JN", "KN", "LN", "MN", "NN", "ON", "PN", "QN", "RN", "SN", "TN", "UN", "VN",
	     "WN", "XN", "YN", "ZN", "AO", "BO", "CO", "DO", "EO", "FO", "GO", "HO", "IO", "JO", "KO", "LO", "MO", "NO", "OO", "PO",
	     "QO", "RO", "SO", "TO", "UO", "VO", "WO", "XO", "YO", "ZO", "AP", "BP", "CP", "DP", "EP", "FP", "GP", "HP", "IP", "JP",
	     "KP", "LP", "MP", "NP", "OP", "PP", "QP", "RP", "SP", "TP", "UP", "VP", "WP", "XP", "YP", "ZP", "AQ", "BQ", "CQ", "DQ",
	     "EQ", "FQ", "GQ", "HQ", "IQ", "JQ", "KQ", "LQ", "MQ", "NQ", "OQ", "PQ", "QQ", "RQ", "SQ", "TQ", "UQ", "VQ", "WQ", "XQ",
	     "YQ", "ZQ", "AR", "BR", "CR", "DR", "ER", "FR", "GR", "HR", "IR", "JR", "KR", "LR", "MR", "NR", "OR", "PR", "QR", "RR",
	     "SR", "TR", "UR", "VR", "WR", "XR", "YR", "ZR", "AS", "BS", "CS", "DS", "ES", "FS", "GS", "HS", "IS", "JS", "KS", "LS",
	     "MS", "NS", "OS", "PS", "QS", "RS", "SS", "TS", "US", "VS", "WS", "XS", "YS", "ZS", "AT", "BT", "CT", "DT", "ET", "FT",
	     "GT", "HT", "IT", "JT", "KT", "LT", "MT", "NT", "OT", "PT", "QT", "RT", "ST", "TT", "UT", "VT", "WT", "XT", "YT", "ZT",
	     "AU", "BU", "CU", "DU", "EU", "FU", "GU", "HU", "IU", "JU", "KU", "LU", "MU", "NU", "OU", "PU", "QU", "RU", "SU", "TU",
	     "UU", "VU", "WU", "XU", "YU", "ZU", "AV", "BV", "CV", "DV", "EV", "FV", "GV", "HV", "IV", "JV", "KV", "LV", "MV", "NV",
	     "OV", "PV", "QV", "RV", "SV", "TV", "UV", "VV", "WV", "XV", "YV", "ZV", "AW", "BW", "CW", "DW", "EW", "FW", "GW", "HW",
	     "IW", "JW", "KW", "LW", "MW", "NW", "OW", "PW", "QW", "RW", "SW", "TW", "UW", "VW", "WW", "XW", "YW", "ZW", "AX", "BX",
	     "CX", "DX", "EX", "FX", "GX", "HX", "IX", "JX", "KX", "LX", "MX", "NX", "OX", "PX", "QX", "RX", "SX", "TX", "UX", "VX",
	     "WX", "XX", "YX", "ZX", "AY", "BY", "CY", "DY", "EY", "FY", "GY", "HY", "IY", "JY", "KY", "LY", "MY", "NY", "OY", "PY",
	     "QY", "RY", "SY", "TY", "UY", "VY", "WY", "XY", "YY", "ZY", "AZ", "BZ", "CZ", "DZ", "EZ", "FZ", "GZ", "HZ", "IZ", "JZ",
	     "KZ", "LZ", "MZ", "NZ", "OZ", "PZ", "QZ", "RZ", "SZ", "TZ", "UZ", "VZ", "WZ", "XZ", "YZ", "ZZ"}) {
		REQUIRE(list[pos] == i);
		pos++;
	}
}

//
// Shuffle tests
//
static constexpr uint64_t SEED1[2] = {0xa8b2f1fc252f3e2e, 0xfbb9b0e565b1979};
static constexpr uint64_t SEED2[2] = {0xbefe3966ef928649, 0x3c5a61b2b7a85dfc};

TEST_CASE("Test string vector shuffle uniqueness", "[shuffle_string_vector][uniqueness]") {
	for(size_t length = 1; length < 5; length++) {
		// Setup values
		vector<string> original = ::search::generate_string_vector(length);
		vector<string> values1  = original;
		vector<string> values2  = original;
		size_t original_size    = original.size();

		// Shuffle
		::search::shuffle_string_vector(values1, SEED1);
		REQUIRE(values1.size() == original_size);
		::search::shuffle_string_vector(values2, SEED2);
		REQUIRE(values2.size() == original_size);

		// Compare uniqueness by seed
		REQUIRE(values1 != values2);

		// Compare uniqueness of entries value
		for(auto v: {&values1, &values2}) {
			::std::unordered_set<string> set(v->begin(), v->end());
			REQUIRE(set.size() == original_size);
			for(auto& s: original) {
				REQUIRE(set.find(s) != set.end());
			}
		}
	}
}
