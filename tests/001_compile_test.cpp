/* tests/001_compile_test.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <search.hpp>

#include <catch2/catch.hpp>
#include <span>
#include <string>
#include <string_view>
#include <type_traits>
#include <vector>

using ::search::container_type;
using ::search::string_container_type;
using ::search::string_type;
using ::search::WordList;
using ::std::span;
using ::std::string;
using ::std::string_view;
using ::std::vector;

TEST_CASE("Test compilecheck string_type", "[string_type]") {
	STATIC_REQUIRE(string_type<char[2]>);
	STATIC_REQUIRE(string_type<span<char>>);
	STATIC_REQUIRE(string_type<span<const char>>);
	STATIC_REQUIRE(string_type<string>);
	STATIC_REQUIRE(string_type<string_view>);

	STATIC_REQUIRE_FALSE(string_type<int[2]>);
	STATIC_REQUIRE_FALSE(string_type<span<int>>);
}

TEST_CASE("Test compilecheck containers string_container_type", "[string_container_type]") {
	STATIC_REQUIRE(string_container_type<vector<string>>);
	STATIC_REQUIRE(string_container_type<vector<string_view>>);
}

TEST_CASE("Test type container value type container_type", "[container_type]") {
	STATIC_REQUIRE(::std::is_same_v<container_type<vector<string>>, string>);
	STATIC_REQUIRE(::std::is_same_v<container_type<vector<string_view>>, string_view>);
}

TEST_CASE("Test compilecheck WordList", "[WordList][compiles]") {
	{
		using TYPE                    = string;
		WordList<TYPE> wordList       = {};
		WordList<TYPE>::iterator iter = {};
		STATIC_REQUIRE(::std::input_or_output_iterator<WordList<TYPE>::iterator>);
		STATIC_REQUIRE(::std::input_iterator<WordList<TYPE>::iterator>);
		STATIC_REQUIRE(::std::ranges::input_range<WordList<TYPE>>);
	}
	{
		using TYPE                    = string_view;
		WordList<TYPE> wordList       = {};
		WordList<TYPE>::iterator iter = {};
		STATIC_REQUIRE(::std::input_or_output_iterator<WordList<TYPE>::iterator>);
		STATIC_REQUIRE(::std::input_iterator<WordList<TYPE>::iterator>);
		STATIC_REQUIRE(::std::ranges::input_range<WordList<TYPE>>);
	}
}
