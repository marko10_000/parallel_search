/* tests/004_word_list.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <search.hpp>
#include <search_generator.hpp>

#include <catch2/catch.hpp>
#include <cstdint>
#include <list>
#include <ranges>
#include <string>
#include <string_view>
#include <vector>

using ::search::WordList;
using ::search::internal::CallHelper;
using ::std::list;
using ::std::string;
using ::std::string_view;
using ::std::vector;

//
// String list generation tests
//
static constexpr uint64_t SEED1[2] = {0xa8b2f1fc252f3e2e, 0xfbb9b0e565b1979};
static constexpr uint64_t SEED2[2] = {0xbefe3966ef928649, 0x3c5a61b2b7a85dfc};

TEST_CASE("Test world list implemenation prefix search all", "[WorldList][all]") {
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WordList<string> result(1);
		CallHelper::run_thread_job_prefix_search(result, original, 0, original.size(), prefix, 0);
		CallHelper::next_iteration(result);

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}
}

TEST_CASE("Test world list implemenation prefix search all in 10 blocks", "[WorldList][all-10blocks]") {
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WordList<string> result(1);
		for(size_t pos = 0; pos < original.size(); pos += 10) {
			CallHelper::run_thread_job_prefix_search(result,
			                                         original,
			                                         pos,
			                                         ((pos + 10 < original.size()) ? pos + 10 : original.size()),
			                                         prefix,
			                                         0);
		}
		CallHelper::next_iteration(result);

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}
}

TEST_CASE("Test world list implemenation prefix search all in 10 blocks 7 semi parallel", "[WorldList][all-10blocks-7parallel]") {
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WordList<string> result(7);
		{
			::search::XorshiftPlus128 randThread({SEED2[0], SEED2[1]});
			for(size_t pos = 0; pos < original.size();) {
				for(size_t _ = 0; _ < 7; _++) {
					if(pos < original.size()) {
						CallHelper::run_thread_job_prefix_search(result,
						                                         original,
						                                         pos,
						                                         ((pos + 10 < original.size()) ? pos + 10 : original.size()),
						                                         prefix,
						                                         randThread.current() % 7);
						randThread.next();
						pos += 10;
					}
				}
				CallHelper::next_iteration(result);
			}
		}

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}
}

TEST_CASE("Test world list implemenation prefix search only DZ", "[WorldList][DZ]") {
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "DZ";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WordList<string> result(1);
		CallHelper::run_thread_job_prefix_search(result, original, 0, original.size(), prefix, 0);
		CallHelper::next_iteration(result);

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}
}

TEST_CASE("Test world list implemenation prefix search only DZ in 10 blocks", "[WorldList][DZ-10blocks]") {
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "DZ";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WordList<string> result(1);
		for(size_t pos = 0; pos < original.size(); pos += 10) {
			CallHelper::run_thread_job_prefix_search(result,
			                                         original,
			                                         pos,
			                                         ((pos + 10 < original.size()) ? pos + 10 : original.size()),
			                                         prefix,
			                                         0);
		}
		CallHelper::next_iteration(result);

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}
}

TEST_CASE("Test world list implemenation prefix search only DZ in 10 blocks 7 semi parallel",
          "[WorldList][DZ-10blocks-7parallel]") {
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "DZ";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WordList<string> result(7);
		{
			::search::XorshiftPlus128 randThread({SEED2[0], SEED2[1]});
			for(size_t pos = 0; pos < original.size();) {
				for(size_t _ = 0; _ < 7; _++) {
					if(pos < original.size()) {
						CallHelper::run_thread_job_prefix_search(result,
						                                         original,
						                                         pos,
						                                         ((pos + 10 < original.size()) ? pos + 10 : original.size()),
						                                         prefix,
						                                         randThread.current() % 7);
						randThread.next();
						pos += 10;
					}
				}
				CallHelper::next_iteration(result);
			}
		}

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}
}
