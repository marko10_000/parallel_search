/* tests/002_prefix_test.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <search.hpp>

#include <catch2/catch.hpp>
#include <span>
#include <string>
#include <string_view>

using ::search::prefix_test;
using ::std::span;
using ::std::string;
using ::std::string_view;

#define STR_DEFINE(STR) STR
TEST_CASE("Test prefix_test implementation (char array)", "[prefix_test][char_array]") {
	REQUIRE(prefix_test(STR_DEFINE(""), STR_DEFINE("")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("a"), STR_DEFINE(""))); // \x00 at the end
	REQUIRE(prefix_test(STR_DEFINE("a"), STR_DEFINE("a")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("a"), STR_DEFINE("b")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE(""), STR_DEFINE("a")));
	REQUIRE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abc")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abcd")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abc"))); // \x00 at the end
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abcD")));
}
#undef STR_DEFINE

#define STR_DEFINE(STR) span<const char>(STR)
TEST_CASE("Test prefix_test implementation (span<const char>)", "[prefix_test][span_const_char]") {
	REQUIRE(prefix_test(STR_DEFINE(""), STR_DEFINE("")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("a"), STR_DEFINE(""))); // \x00 at the end
	REQUIRE(prefix_test(STR_DEFINE("a"), STR_DEFINE("a")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("a"), STR_DEFINE("b")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE(""), STR_DEFINE("a")));
	REQUIRE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abc")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abcd")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abc"))); // \x00 at the end
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abcD")));
}
#undef STR_DEFINE

#define STR_DEFINE(STR) string(STR)
TEST_CASE("Test prefix_test implementation (string)", "[prefix_test][string]") {
	REQUIRE(prefix_test(STR_DEFINE(""), STR_DEFINE("")));
	REQUIRE(prefix_test(STR_DEFINE("a"), STR_DEFINE("")));
	REQUIRE(prefix_test(STR_DEFINE("a"), STR_DEFINE("a")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("a"), STR_DEFINE("b")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE(""), STR_DEFINE("a")));
	REQUIRE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abc")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abcd")));
	REQUIRE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abc")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abcD")));
}
#undef STR_DEFINE

#define STR_DEFINE(STR) string_view(STR)
TEST_CASE("Test prefix_test implementation (string_view)", "[prefix_test][string_view]") {
	REQUIRE(prefix_test(STR_DEFINE(""), STR_DEFINE("")));
	REQUIRE(prefix_test(STR_DEFINE("a"), STR_DEFINE("")));
	REQUIRE(prefix_test(STR_DEFINE("a"), STR_DEFINE("a")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("a"), STR_DEFINE("b")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE(""), STR_DEFINE("a")));
	REQUIRE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abc")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abc"), STR_DEFINE("abcd")));
	REQUIRE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abc")));
	REQUIRE_FALSE(prefix_test(STR_DEFINE("abcd"), STR_DEFINE("abcD")));
}
#undef STR_DEFINE
