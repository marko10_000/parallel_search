/* tests/006_prefix_search.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <chrono>
#include <iostream>
#include <search.hpp>
#include <search_generator.hpp>

#include <catch2/catch.hpp>
#include <cstdint>
#include <list>
#include <ranges>
#include <string>
#include <string_view>
#include <thread>
#include <vector>

using ::search::prefix_search;
using ::search::prefix_search_timed;
using ::std::list;
using ::std::string;
using ::std::string_view;
using ::std::thread;
using ::std::vector;

//
// String list generation tests
//
static constexpr uint64_t SEED1[2] = {0xa8b2f1fc252f3e2e, 0xfbb9b0e565b1979};
static constexpr uint64_t SEED2[2] = {0xbefe3966ef928649, 0x3c5a61b2b7a85dfc};

TEST_CASE("Test prefix search with all results from single vs multithreaded.", "[prefix_search_timed][all]") {
	// Generate input
	vector<string> original = ::search::generate_string_vector(4);
	::search::shuffle_string_vector(original, SEED1);
	string_view prefix("");

	// Process content
	double time1;
	auto res1 = prefix_search_timed(original, prefix, time1, 1);
	double time2;
	auto res2 = prefix_search_timed(original, prefix, time2, 4);
	::std::cout << "Threads 1: " << time1 << " | Threads 4: " << time2 << ::std::endl;

	// Check content
	REQUIRE(res1.size() == res2.size());
	{
		size_t counted = 0;
		auto r1_pos    = res1.begin();
		auto r2_pos    = res2.begin();
		while((r1_pos != res1.end()) && (r2_pos != res2.end())) {
			REQUIRE(*r1_pos == *r2_pos);
			r1_pos++;
			r2_pos++;
			counted++;
		}
		REQUIRE(res1.size() == counted);
		REQUIRE(r1_pos == res1.end());
		REQUIRE(r2_pos == res2.end());
	}
}

TEST_CASE("Test prefix search with only DZ results from single vs multithreaded.", "[prefix_search_timed][DZ]") {
	// Generate input
	vector<string> original = ::search::generate_string_vector(4);
	::search::shuffle_string_vector(original, SEED1);
	string_view prefix("DZ");

	// Process content
	double time1;
	auto res1 = prefix_search_timed(original, prefix, time1, 1);
	double time2;
	auto res2 = prefix_search_timed(original, prefix, time2, 4);
	::std::cout << "Threads 1: " << time1 << " | Threads 4: " << time2 << ::std::endl;

	// Check content
	REQUIRE(res1.size() == res2.size());
	{
		size_t counted = 0;
		auto r1_pos    = res1.begin();
		auto r2_pos    = res2.begin();
		while((r1_pos != res1.end()) && (r2_pos != res2.end())) {
			REQUIRE(*r1_pos == *r2_pos);
			r1_pos++;
			r2_pos++;
			counted++;
		}
		REQUIRE(res1.size() == counted);
		REQUIRE(r1_pos == res1.end());
		REQUIRE(r2_pos == res2.end());
	}
}

TEST_CASE("Test prefix search with only DZ results from single vs multithreaded 64", "[prefix_search_timed][DZ]") {
	// Generate input
	vector<string> original = ::search::generate_string_vector(4);
	::search::shuffle_string_vector(original, SEED1);
	string_view prefix("DZ");

	// Process content
	double time1;
	auto res1 = prefix_search_timed<64>(original, prefix, time1, 1);
	double time2;
	auto res2 = prefix_search_timed<64>(original, prefix, time2, 4);
	::std::cout << "64 data blocks: Threads 1: " << time1 << " | Threads 4: " << time2 << ::std::endl;

	// Check content
	REQUIRE(res1.size() == res2.size());
	{
		size_t counted = 0;
		auto r1_pos    = res1.begin();
		auto r2_pos    = res2.begin();
		while((r1_pos != res1.end()) && (r2_pos != res2.end())) {
			REQUIRE(*r1_pos == *r2_pos);
			r1_pos++;
			r2_pos++;
			counted++;
		}
		REQUIRE(res1.size() == counted);
		REQUIRE(r1_pos == res1.end());
		REQUIRE(r2_pos == res2.end());
	}
}

TEST_CASE("Test prefix search with only DZ results from single vs multithreaded without time.", "[prefix_search][DZ]") {
	// Generate input
	vector<string> original = ::search::generate_string_vector(4);
	::search::shuffle_string_vector(original, SEED1);
	string_view prefix("DZ");

	// Process content
	auto res1 = prefix_search(original, prefix, 1);
	auto res2 = prefix_search(original, prefix, 4);

	// Check content
	REQUIRE(res1.size() == res2.size());
	{
		size_t counted = 0;
		auto r1_pos    = res1.begin();
		auto r2_pos    = res2.begin();
		while((r1_pos != res1.end()) && (r2_pos != res2.end())) {
			REQUIRE(*r1_pos == *r2_pos);
			r1_pos++;
			r2_pos++;
			counted++;
		}
		REQUIRE(res1.size() == counted);
		REQUIRE(r1_pos == res1.end());
		REQUIRE(r2_pos == res2.end());
	}
}
