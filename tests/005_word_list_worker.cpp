/* tests/005_word_list_worker.cpp
 *
 * Copyright 2022 Marko Semet <marko.semet@mailbox.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include <chrono>
#include <search.hpp>
#include <search_generator.hpp>

#include <catch2/catch.hpp>
#include <cstdint>
#include <list>
#include <ranges>
#include <string>
#include <string_view>
#include <thread>
#include <vector>

using ::search::WordList;
using ::search::WordListWorker;
using ::search::internal::CallHelper;
using ::std::list;
using ::std::string;
using ::std::string_view;
using ::std::thread;
using ::std::vector;

//
// String list generation tests
//
static constexpr uint64_t SEED1[2] = {0xa8b2f1fc252f3e2e, 0xfbb9b0e565b1979};
static constexpr uint64_t SEED2[2] = {0xbefe3966ef928649, 0x3c5a61b2b7a85dfc};

TEST_CASE("Test WordListWorker to exit thread correctly", "[WordListWorker][empty-exit]") {
	// Define threads
	thread t1;
	thread t2;

	// Create list
	using WL  = WordList<string>;
	using WLW = WordListWorker<WL, vector<string>, string_view>;
	WLW wlw;

	// Make thread working
	t1 = thread([](WLW* wlw) -> void { wlw->thread_join_pool(); }, &wlw);
	t2 = thread([](WLW* wlw) -> void { wlw->thread_join_pool(); }, &wlw);
	::std::this_thread::sleep_for(::std::chrono::milliseconds(100));
	wlw.exit();

	// Join threads
	t1.join();
	t2.join();
}

TEST_CASE("Test WordListWorker to process all", "[WordListWorker][all]") {
	// Define threads
	thread t1;
	thread t2;

	// Create list
	using WL  = WordList<string>;
	using WLW = WordListWorker<WL, vector<string>, string_view>;
	WLW wlw;

	// Make thread working
	t1 = thread([](WLW* wlw) -> void { wlw->thread_join_pool(); }, &wlw);
	t2 = thread([](WLW* wlw) -> void { wlw->thread_join_pool(); }, &wlw);

	// Process data
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WL result(2);
		wlw.apply_prefix_job(result, original, prefix);
		wlw.await_workers_done();

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}

	// Join threads
	wlw.exit();
	t1.join();
	t2.join();
}

TEST_CASE("Test WordListWorker to process DZ", "[WordListWorker][DZ]") {
	// Define threads
	thread t1;
	thread t2;

	// Create list
	using WL  = WordList<string>;
	using WLW = WordListWorker<WL, vector<string>, string_view>;
	WLW wlw;

	// Make thread working
	t1 = thread([](WLW* wlw) -> void { wlw->thread_join_pool(); }, &wlw);
	t2 = thread([](WLW* wlw) -> void { wlw->thread_join_pool(); }, &wlw);

	// Process data
	for(size_t length = 1; length < 5; length++) {
		// Generate original
		vector<string> original = ::search::generate_string_vector(length);
		::search::shuffle_string_vector(original, SEED1);

		// Generate expected result
		::std::string_view prefix = "DZ";
		auto r                    = ::std::ranges::ref_view(original)
		         | ::std::views::filter([&prefix](string& s) -> bool { return ::search::prefix_test(s, prefix); });
		list<string> expected(r.begin(), r.end());

		// Generate word list result
		WL result(2);
		wlw.apply_prefix_job(result, original, prefix);
		wlw.await_workers_done();

		// Compare results
		REQUIRE(result.size() == expected.size());
		{
			size_t counted = 0;
			auto e_pos     = expected.begin();
			auto wl_pos    = result.begin();
			while((e_pos != expected.end()) && (wl_pos != result.end())) {
				REQUIRE(*e_pos == *wl_pos);
				e_pos++;
				wl_pos++;
				counted++;
			}
			REQUIRE(expected.size() == counted);
			REQUIRE(wl_pos == result.end());
		}
	}

	// Join threads
	wlw.exit();
	t1.join();
	t2.join();
}
